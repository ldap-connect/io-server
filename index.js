var express = require("express"), http = require('http');
var app = express();
var server = http.createServer(app);
var port  = 3000;
var io = require('socket.io').listen(server);
server.listen(port);

io.on('connection',(socket)=>{

    console.log("new connection");

    socket.on('callMethod',function(data){

        console.log("index.js :: server :: 1" + {data});
        socket.join(data);

        socket.broadcast.emit('emitEvent',data);
     
    });

});